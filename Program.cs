﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Otus_DZ4
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Введите URL или exit для выхода: ");
                string url = Console.ReadLine();//url = "https://kolesa.kz";
                if (url == String.Empty)
                {
                    Console.WriteLine("Введите url-адрес");
                    continue;
                }
                else if (url.Equals("exit", StringComparison.OrdinalIgnoreCase))
                    return;
                WebClient webclient = new WebClient();
                HtmlText text = new HtmlText(webclient);
                string html = text.GetHtmlText(url);
                //Console.WriteLine(html);
                HtmlParser parserTextHtml = new HtmlParser();
                Dictionary<string, string> img = parserTextHtml.GetListImg(html);
                Console.WriteLine($"Найдено {img.Count} изображений");
                Download load = new Download();
                load.DownloadFileAsync(img, url, @"..\..\..\img").GetAwaiter();
            }
        }
    }
}
