﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Otus_DZ4
{
    class HtmlText
    {
        private readonly WebClient _webclient;

        public HtmlText(WebClient webclient)
        {
            _webclient = webclient;
        }

        public string GetHtmlText(string url)
        {
            string str;
            using (WebClient client = _webclient)
            {
                WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultCredentials;

                try
                {
                    string webPagestr = client.DownloadString(url);
                    return webPagestr;
                }
                catch (Exception e)
                {
                    str = "FAIL" + e;
                }
            }
            return str;
        }
    }
}
